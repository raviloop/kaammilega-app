angular.module('app.directives', ['app.services'])

/*.directive('blankDirective', [function(){

}]);
*/

.filter('capitalize', function() {
	return function(input, all) {
		var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
		return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
	}
})

.directive('formattedTime', function ($filter) {

	return {
		require: '?ngModel',
		link: function(scope, elem, attr, ngModel) {
			if( !ngModel )
				return;
			if( attr.type !== 'time' )
				return;

			ngModel.$formatters.unshift(function(value) {
				return value.replace(/:[0-9]+.[0-9]+$/, '');
			});
		}
	};

})
